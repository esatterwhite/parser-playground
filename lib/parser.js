'use strict'

const {CstParser, EmbeddedActionsParser} = require('chevrotain')
const tokens = require('./tokens')
const {lexerDefinition} = require('./lexer')

class SelectParser extends CstParser {
  constructor() {
    super(lexerDefinition)

    const $ = this
    $.RULE('selectStatement', () => {
      $.SUBRULE($.selectClause)
      $.SUBRULE($.fromClause)
      $.OPTION(() => {
        $.SUBRULE($.whereClause)
      })
    })

    $.RULE('fromClause', () => {
      $.CONSUME(tokens.FROM)
      $.CONSUME(tokens.IDENTIFIER)
    })

    $.RULE('whereClause', () => {
      $.CONSUME(tokens.WHERE)
      $.AT_LEAST_ONE(() => {
        $.SUBRULE($.condition)
      })
    })

    $.RULE('condition', () => {

      $.SUBRULE($.expression)
    })

    $.RULE('expression', () => {
      $.SUBRULE($.atomicExpression, { LABEL: 'lhs' })
      $.SUBRULE($.relationalOperator)
      $.SUBRULE2($.atomicExpression, { LABEL: 'rhs' })
    })

    $.RULE('selectClause', () => {
      $.CONSUME(tokens.SELECT)
      $.AT_LEAST_ONE_SEP({
        SEP: tokens.COMMA
      , DEF: () => {
          $.CONSUME(tokens.IDENTIFIER)
        }
      })
    })

    // atomic expression
    //   : INTEGER | IDENTIFIER
    $.RULE('atomicExpression', () => {
      $.OR([
        { ALT: () => {
            $.CONSUME(tokens.INTEGER)
          }
        }
      , { ALT: () => {
            $.CONSUME(tokens.IDENTIFIER)
          }
        }
      ])
    })

    $.RULE('relationalOperator', () => {
      $.OR([
        { ALT: () => {
            $.CONSUME(tokens.GREATER_THAN)
          }
        }
      , { ALT: () => {
            $.CONSUME(tokens.LESS_THAN)
          }
        }
      , { ALT: () => {
            $.CONSUME(tokens.EQUAL_TO)
          }
        }
      ])
    })

    this.performSelfAnalysis()
  }
}

module.exports = SelectParser
