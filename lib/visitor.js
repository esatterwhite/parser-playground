'use strict'

const Parser = require('./parser')
const Visitor = (new Parser).getBaseCstVisitorConstructor()

class SQLVisitor extends Visitor {
  constructor() {
    super()
    this.validateVisitor()
  }

  selectStatement(ctx) {
    let select = this.visit(ctx.selectClause)
    return {
      type: 'SELECT_STATEMENT'
    , selectClause: this.visit(ctx.selectClause)
    , fromClause: this.visit(ctx.fromClause)
    , whereClause: this.visit(ctx.whereClause)
    }
  }

  selectClause(ctx) {
    return {
      type: 'SELECT_CLAUSE'
    , columns: ctx.Identifier.map((i) => {
        return {
          type: 'column'
        , value: i.image
        }
      })
    }
  }

  fromClause(ctx) {
    return {
      type: 'FROM_CLAUSE'
    , table: ctx.Identifier[0].image
    }
  }

  whereClause(ctx) {
    const tree = ctx.condition.reduce((expressions, current) => {
      debugger;
      if (!expressions.expressionTree) {
        const tree = this.visit(current)
        expressions.prevExpression = tree
        expressions.expressionTree = tree
      } else {
        const next = this.visit(current, {
          prevExpression: expressions.prevExpression
        });
        expressions.prevExpression.right = next
        expressions.prevExpression = expressions.prevExpression.right;
      }
      return expressions
    }, {prevExpression: null, expressionTree: null})

    debugger
    return {
      type: 'WHERE_CLAUSE'
    , children: tree.expressionTree
    }
  }

  condition(ctx) {
    console.log(ctx.expression)
    return {type: 'CONDITION'}
  }
  expression(ctx) {
    return {
      type: 'EXPRESSION'
    , lhs: this.visit(ctx.lhs[0])
    , operator: this.visit(ctx.relationalOperator)
    , rhs: this.visit(ctx.rhs[0])
    }
  }

  relationalOperator(ctx) {
    if (ctx.GreaterThan) return ctx.GreaterThan[0].image
    return ctx.LessThan[0].image
  }

  atomicExpression(ctx) {
    if (ctx.Integer) return ctx.Integer[0].image
    return ctx.Identifier[0].image
  }
}

module.exports = SQLVisitor
