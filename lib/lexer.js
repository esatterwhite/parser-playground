'use strict'

const {createToken, Lexer} = require('chevrotain')
const tokens = require('./tokens')

const lexer = new Lexer([
  tokens.WHITE_SPACE
  // Keywords
, tokens.SELECT
, tokens.FROM
, tokens.WHERE
, tokens.COMMA

  // Identifiers
, tokens.IDENTIFIER
, tokens.INTEGER
, tokens.DECIMAL
, tokens.GREATER_THAN
, tokens.LESS_THAN
, tokens.EQUAL_TO
])

module.exports = lexer
