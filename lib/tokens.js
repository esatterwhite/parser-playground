'use strict'

const {createToken, Lexer} = require('chevrotain')

const WHITE_SPACE = createToken({
  name: "WhiteSpace"
, pattern: /\s+/
, group:Lexer.SKIPPED
})

const COMMA = createToken({
  name: 'Comma'
, pattern: /,/
})

const GREATER_THAN = createToken({
  name: 'GreaterThan'
, pattern: />/
})

const LESS_THAN = createToken({
  name: 'LessThan'
, pattern: /</
})

const EQUAL_TO = createToken({
  name: 'EqualTo'
, pattern: /=/
})

const IDENTIFIER = createToken({
  name: 'Identifier'
, pattern: /[a-zA-Z]\w*/
})

const FROM = createToken({
  name: 'From'
, pattern: /FROM/
})

const SELECT = createToken({
  name: 'Select'
, pattern: /SELECT/
, longer_alt: IDENTIFIER
})

const WHERE = createToken({
  name: 'Where'
, pattern: /WHERE/
, longer_alt: IDENTIFIER
})

const INTEGER = createToken({
  name: 'Integer'
, pattern: /0|[1-9]\d*/
})

const DECIMAL = createToken({
  name: 'Decimal'
, pattern: /\d+\.\d+/
})

const AND = createToken({
  name: 'And'
, pattern: /\bAND\b/
})

const OR = createToken({
  name: 'Or'
, pattern: /\bOR\b/
})

const NOT = createToken({
  name: 'NOT'
, pattern: /\bNOT\b/
})

module.exports = {
  WHITE_SPACE
  // Keywords
, SELECT
, FROM
, WHERE
, COMMA

  // Identifiers
, IDENTIFIER
, DECIMAL
, INTEGER
, AND
, OR
, NOT
, GREATER_THAN
, LESS_THAN
, EQUAL_TO
}
