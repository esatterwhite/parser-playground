'use strict'

const {
  EmbededActionsParser
, createToken
} = require('chevrotain')

const {DECIMAL, INTEGER} = require('./tokens')

class SelectParser extends EmbededActionsParser {
  constructor() {
    super(tokens)
    const $ = this

    $.RULE('topRule', () => {
      let result = 0
      $.many(() => {
        $.OR([
          { ALT: () => {
              result += $.SUBRULE($.decimalRule)
            }
          }
        , { ALT: () => {
              result += $.SUBRULE($.integerRule)
            }
          }
        ])
      })

      return result
    })

    $.RULE('decimalRule', () => {
      const token = $.CONSUME(DECIMAL)
      return parseFloat(decimalToken.image)
    })

    $.RULE('integerRule', () => {
      const token = $.CONSUME(INTEGER)
      return parseInt(token.image)
    })
  }
}
