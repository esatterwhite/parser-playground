LogDNA Backend Engineering Challenge (parser)
=====================================

The goal of this challenge is to assess your coding ability and get a feel for how you deliver products.

You will be judged on all aspects of your deliverable, think of it as a gateway for us to see how you work.

----
The Problem
----
Good user experience is core to what we do here at LogDNA. So we try to build features that taylor to our user's needs. In many cases, this means making these features flexible and susceptible to human error. In this particular challenge, we are looking to improve user experience by doing some parsing of search terms for JSON fields.

Today, we do automatic parsing of JSON data upon ingestion. This allows the data to be searched on by JSON field names. However, we are still fairly primitive in this area. We would like to be better at this, and we will, but let's see if you're up to the task first.

In order for our search to run, we need to translate plain text queries into some form of JSON format that can then be consumed by services to perform search or filtering down the line. In this challenge, your job is to write a REST API service that takes as input a plain text string and as output generates a JSON that describes the search rule.

Search rules:
- terms are implicitly AND'd together unless quoted
- terms are implicitly an exact match
- multiple search terms can be nested using ()'s
- negation can be done using ! in front of search term
- OR'ing search terms can be done by explicitly using "OR" keyword
- AND'ing search terms can optionally be done by explicitly using "AND" keyword
- using '>', '>=', '<', '=<' denotes a non exact match on the term following respective symbol
- using '=' denotes an exact match on the term following respective symbol
- len(#) will allow us to match length of JSON data instead of actual value
- 'true', 'false' will be matched to their boolean values instead of string values

Some example input/outputs:

:error OR info
> { "$or": ["error", "info"] }

:>400 <500
> { "$and": [
    { "$gt": "400" }
  , { "$lt": "500" }
] }

:="TEST DATA" OR >len(9)
> { "$or" [
    {
        "$eq": {
            "$quoted": "TEST DATA"
        }
    }, {
        "$gt": {
            "$len": 9
        }
    }
] }

:!false
> { "$not": false }


As for the REST API portion, you can choose how best to do this. Please use one of the following languages: python, ruby, node or go-lang. (We would obviously prefer node as that's what we use here).

Feel free to make any assumptions along the way AS LONG AS YOU DOCUMENT IT. Imagine you are throwing your service over the fence to "the other guys", we really need to know how to use it and what the constraints are.

We appreciate *resourcefulness*, so feel free to scour the interwebs for anything and everything that can help you get the job done.

Lastly, you might feel there is a better solution to what is proposed here, and can come up with an even better experience, we're all ears. Challenge us, persuade us. That's what we're all about.
